import {Component, EventEmitter, Input, Output} from '@angular/core';
import {newOrder, Order} from "../shared/order.modal";

@Component({
  selector: 'app-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.css']
})
export class AddItemsComponent {
  @Input() orders!: Order[];
  @Input() order!: Order;
  @Output() addItem = new EventEmitter();

  onAddItems() {
    this.addItem.emit();
  }


}
