import {Component, EventEmitter, Input, Output} from '@angular/core';
import {newOrder, Order} from "../shared/order.modal";

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent {
  @Input() newOrder!: newOrder;
  @Output() delete = new EventEmitter();


  getTotalPrice(){
    const total = this.newOrder.number * this.newOrder.price;
    return total;
  }

  onDeleteOrder() {
    this.delete.emit()
  }
}
