import {Component} from '@angular/core';
import {newOrder, Order} from "./shared/order.modal";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  newOrders: newOrder[] = [];

  orders: Order[] = [
    new Order('hamburger', 0, 80),
    new Order('cheeseburger', 0, 90),
    new Order('fries', 0, 60),
    new Order('Coffee', 0, 80),
    new Order('Tea', 0, 50),
    new Order('Coca-Cola', 0, 40),
  ];


  onNewOrder(index: number){
    this.newOrders.push(this.orders[index]);
    this.orders[index].number++;
  }


  onDelete(index: number){
    this.newOrders.splice(index, 1);
  }
}
