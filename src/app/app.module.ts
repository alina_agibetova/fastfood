import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AddItemsComponent } from './add-items/add-items.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { NewOrderComponent } from './new-order/new-order.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    AddItemsComponent,
    OrderDetailsComponent,
    NewOrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
